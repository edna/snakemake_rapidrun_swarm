#!/usr/bin/env python3
__author__ = "Morgane Bruno"
__licence__ = "MIT"

#_libs
import pandas as pd
import sys, os, warnings, logging
import ast
from Bio.Seq import Seq
from pathlib import Path

#_main
def main(argv) -> None:
    #args
    demulti_file = snakemake.input[0]   #path/all_demultiplex.csv
    demultiplex_tag = snakemake.params.header_fasta   #results/intermediates/03_demultiplex_tag/
    out_path = os.path.dirname(snakemake.output.summ)   #results/intermediates/settings/
    log = snakemake.log[0]

    #logs
    logging.basicConfig(format = '%(asctime)s :: %(levelname)s :: %(message)s',
                        filename = log,
                        filemode = 'w',
                        level = logging.INFO)

    demulti = pd.read_csv(demulti_file)
    write_barcodes_fasta(demulti, out_path, demultiplex_tag)
    logging.captureWarnings(True)

#_fun
def write_barcodes_fasta(demulti: pd.pandas.core.frame.DataFrame, out_path: str, demultiplex_path: str) -> None:
    ''' Write fasta files with barcodes for each run

    Args:
        demulti (Pandas DataFrame): dataframe all_demultiplex
        out_path (str): path to output folder
        demultiplex_path (str): path to demultiplex_tag folder
    '''
    uniq_run = demulti.run.unique()
    summ_file = Path(out_path, 'barcodes_summary.csv')
    summ = open(summ_file, 'w')
    for run in uniq_run:
        demulti_run = demulti[demulti.run == run]
        out_file = Path(out_path, f'{run}.fasta')
        barcode_file = open(out_file, 'w')
        logging.info(f'Writting {barcode_file}')
        for index, row in demulti_run.iterrows():
            header = f'>{Path(demultiplex_path, row.demultiplex)}'
            sequence = f'{row.barcode5}...{row.barcode3}'
            barcode_file.write(f'{header}\n')
            barcode_file.write(f'{sequence}\n')
            summ.write(f'{header},{sequence}\n')
        barcode_file.close()
    summ.close()

if __name__ == '__main__':
    main(sys.argv)
