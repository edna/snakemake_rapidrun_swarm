__author__ = "Morgane Bruno"
__license__ = "MIT"

## Create file describing demultiplex data
rule demultiplex_data:
    input:
        config['files']['all_samples']
    output:
        Path('results', config['subfolders']['settings'], 'all_demultiplex.csv')
    log:
        Path('logs', config['subfolders']['settings'], 'all_demultiplex.log')
    conda:
        '../envs/env_scripts_python.yaml'
    resources:
        job = 1
    threads: 1
    params:
        dir_primers_tags = Path('results', config['subfolders']['demultiplex_tag']),
        blackl = json.loads(json.dumps(config['blacklist'])),
        dat = json.loads(json.dumps(config['files']['dat']))
    message:
        '(python script) Create file describing demultiplex data'
    script: '../scripts/write_demultiplex_files.py'

## Create barcodes fasta for each run
rule write_barcodes_files:
    input:
        rules.demultiplex_data.output
    output:
        summ = Path('results', config['subfolders']['settings'], 'barcodes/barcodes_summary.csv'),
        fa = expand(Path('results', config['subfolders']['settings'], 'barcodes/{run}.fasta').as_posix(), run = list_run())
    log:
        Path('logs', config['subfolders']['settings'], 'barcodes_summary.log')
    conda:
        '../envs/env_scripts_python.yaml'
    resources:
        job = 1
    threads: 1
    params:
        header_fasta = f'results/{config["subfolders"]["demultiplex_tag"]}'
    message:
        '(python script) Write barcodes files'
    script: '../scripts/write_barcodes_files.py'

## Run demultiplexing for all fastq files
rule demultiplex_tag:
    input:
        fq = expand(Path('results', config['subfolders']['merge_fastq'], '{run}.fastq').as_posix(), run = list_run()),
        barcode = rules.write_barcodes_files.output.fa
    output:
        expand(Path('results', config['subfolders']['demultiplex_tag'], '{demultiplex}.fastq').as_posix(), demultiplex = list_demultiplex())
    log:
        Path('logs', config['subfolders']['demultiplex_tag'], 'cutadapt_demultiplexing.log')
    conda:
        '../envs/env_cutadapt.yaml'
    container:
        config["singularity"]["ednatools"]
    resources:
        job = 1
    threads: workflow.cores * 0.5
    params:
        min_len = config['cutadapt']['min_len'],
        all_run = list_run()
    message:
        '(cutadapt) demultiplexing'
    shell:
        '''
        for file in {output}
        do
            mkdir -p "$(dirname $file)"
        done
        bash scripts/cutadapt_demultiplexing.sh $(dirname {input.barcode[0]}) $(dirname {input.fq[0]}) {threads} {params.min_len} {params.all_run} &>> {log}
        for file in {output}
        do
            if [ ! -f $file ]
            then
                touch $file
                echo "warnings :: "$file" is not created. Generating empty files" >> {log}
            fi
        done
        '''
