__author__ = "Morgane Bruno"
__licence__ = "MIT"

# --------------------------------- Functions ---------------------------------#
def get_adapter(wildcards, input):
    with open(f'{input.csv}', 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(',')
            demultiplex = line_splitted[0]
            if demultiplex == wildcards.demultiplex:
                primer5 = line_splitted[8]
                primer3 =  line_splitted[9]
                return f'^{primer5}...{primer3}$'

def list_run():
    all_run = []
    with open(config['files']["all_samples"]) as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            run = line_splitted[1]
            if not run in all_run:
                all_run.append(run)
    return(all_run)

def list_demultiplex():
    all_demultiplex = []
    with open(config['files']["all_samples"]) as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            run = line_splitted[1]
            sample = line_splitted[2]
            all_demultiplex.append(f'{projet}/{marker}/{sample}')
    return(all_demultiplex)

def list_projmark():
    all_projmark = []
    with open(config['files']["all_samples"]) as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            projmark = f'{projet}/{marker}'
            if not projmark in all_projmark:
                all_projmark.append(projmark)
    return(all_projmark)

def get_sample_fasta(wildcards):
    sample_fasta = []
    for file in expand(Path('results', config['subfolders']['sample_dereplicate'], '{demultiplex}.fasta').as_posix(), demultiplex = list_demultiplex()):
        if wildcards.projmark in file:
            sample_fasta.append(file)
    return sample_fasta

def get_custom_assignment_input(wildcards):
    with open(config['files']['all_samples'], 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            pm = f'{projet}/{marker}'
            if marker in config['custom_ref']:
                if pm in {wildcards.projmark}:
                    return  Path('results', config["subfolders"]["otu_fasta"], pm, 'all.otu.fasta')

def get_bdr(wildcards):
    marker = list({wildcards.projmark})[0].split('/')[1]
    folder = config['reference'][marker]['folder']
    prefix = config['reference'][marker]['prefix']
    return Path(folder, prefix).as_posix()

### Targets
def get_targets():
    files = []
    with open(config['files']['all_samples'], 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(';')
            projet = line_splitted[3]
            marker = line_splitted[4]
            run = line_splitted[1]
            pipeline = config['subfolders']['table_assignment']
            rpm = f'results/{pipeline}/{projet}/{marker}/'
            if projet not in config['blacklist']['projet'] and run not in config['blacklist']['run']:
                tsv = f'{rpm}{projet}_{marker}_otu_genbank_ecotag.tsv'
                table = f'{rpm}{projet}_{marker}_table_motu.tsv'
                if table not in files:
                    files.append(table)
                if tsv not in files:
                    files.append(tsv)
                    if config['custom_baseref'] and marker in config['custom_ref']:
                        files.append(f'{rpm}{projet}_{marker}_otu_custom_ecotag.tsv')
    return files
