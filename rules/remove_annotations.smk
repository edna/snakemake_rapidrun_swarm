__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## Some unuseful attributes can be removed at this stage
rule remove_annotations:
    input:
        Path('results', config['subfolders']['taxonomic_assignment'], '{projmark}/otu_{bdr}.tag.u.fasta')
    output:
        Path('results', config['subfolders']['remove_annotations'], '{projmark}/otu_{bdr}.ann.fasta')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['remove_annotations'], '{projmark}/otu_{bdr}.ann.log')
    shell:
        '''
        mkdir -p $(dirname {output});
        obiannotate --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
        --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
        --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
        --delete-tag=obiclean_head --delete-tag=obiclean_headcount \
        --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
        --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
        --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
        --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
        --delete-tag=tail_quality {input} > {output} 2> {log}
        '''
