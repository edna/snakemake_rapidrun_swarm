__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__licence__ = "MIT"

## Merge strictly identical sequences contained in one sample
rule sample_dereplicate_sequences:
    input:
        rules.discard_iupac_ambiguous_sequences.output
    output:
        Path('results', config['subfolders']['sample_dereplicate'], '{demultiplex}.fasta')
    log:
        Path('logs', config['subfolders']['sample_dereplicate'], '{demultiplex}.log')
    conda:
        '../envs/env_vsearch.yaml'
    container:
    	config['singularity']['ednatools']
    resources:
        job = 1
    threads: 1
    message:
        '(vsearch): Merge strictly identical sequences contained in {wildcards.demultiplex}'
    shell:
        '''
        mkdir -p $(dirname {output})
        if [ -s {input} ]
        then
            vsearch --derep_fulllength {input} --sizeout --fasta_width 0 --relabel_sha1 --output {output} &> {log}
        else
            touch {output}
            echo "The input file {input} is empty." >> {log}
        fi
        '''
