__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## cutadapt - remove adapter sequences from high-throughput sequencing reads
rule trim_primers:
    input:
        fq = Path('results', config['subfolders']['demultiplex_tag'], '{demultiplex}.fastq'),
        csv = rules.demultiplex_data.output
    output:
        Path('results', config['subfolders']['trim_primers'], '{demultiplex}.fastq')
    log:
        Path('logs', config['subfolders']['trim_primers'], '{demultiplex}.log')
    container:
    	config['singularity']['ednatools']
    conda:
        '../envs/env_cutadapt.yaml'
    resources:
        job = 1
    threads: 4
    params:
        max_len = config['cutadapt']['max_len'],
        min_len = config['cutadapt']['min_len'],
        adapter = lambda w, input: get_adapter(w, input)
    message:
        '(cutadapt): remove adapter sequences'
    shell:
        '''
        mkdir -p $(dirname {output})
        if [ -s {input.fq} ]
        then
            cutadapt --no-indels --discard-untrimmed -m {params.min_len} -M {params.max_len} -a {params.adapter} {input.fq} > {output} 2> {log}
        else
            touch {output}
            echo "The input file {input.fq} is empty." >> {log}
        fi
        '''
