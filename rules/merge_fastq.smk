__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__licence__ = "MIT"

## Merge read pairs
rule merge_fastq:
    input:
        fw = Path(config['files']['folder_fastq'], '{run}_R1.fastq.gz'),
        rv = Path(config['files']['folder_fastq'], '{run}_R2.fastq.gz')
    output:
        Path('results', config['subfolders']['merge_fastq'], '{run}.fastq')
    log:
        Path('logs', config['subfolders']['merge_fastq'], '{run}.log')
    conda:
        '../envs/env_vsearch.yaml'
    container:
    	config["singularity"]["ednatools"]
    resources:
        job = 1
    threads: 4
    params:
        encoding = config['vsearch']['encoding'],
    message:
        '(vsearch) Merge reads: {wildcards.run}'
    shell:
        '''
        vsearch --threads {threads} \
        --fastq_mergepairs {input.fw} --reverse {input.rv} \
        --fastqout {output} \
        --fastq_ascii {params.encoding} --fastq_allowmergestagger --quiet 2> {log}
        '''
