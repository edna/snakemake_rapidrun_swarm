###############################################################################
## Cutadapt don't demultiplex twice the same barcode
## With rapidrun protocol, they are many times the same barcode into a run
## SO we simply copy paste the duplicated barcode that are not procedeed
##
##
## Authors : GUERIN Pierre-Edouard, MARQUES Virginie
## Montpellier 2019-2020
###############################################################################

RUN=$1
#RUN=../results/01_settings/barcodes/181102_SND405_A_L002_AIMI-49_duplicated.csv

for i in `cat $RUN`;
do
ORIGINAL=`echo $i | cut -d, -f1`
DUPLICATED=`echo $i | cut -d, -f2`
cp $ORIGINAL $DUPLICATED
done