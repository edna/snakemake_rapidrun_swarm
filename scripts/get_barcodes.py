#!/usr/bin/en python3
__author__ = "Morgane Bruno"
__licence__ = "MIT"

import sys

def main(argv) -> None:
    demulti_path = argv[1]
    projet = argv[2]
    marker = argv[3]
    run = argv[4]
    pmr = f'{projet}/{marker}/{run}'
    barcodes = get_barcodes(demulti_path, pmr)
    print(barcodes)


def get_barcodes(demulti: str, pmr: str) -> str:
    with open(demulti, 'r') as f:
        for line in f:
            line_splitted = line.strip('\n').split(',')
            demultiplex = f'{line_splitted[1]}/{line_splitted[2]}/{line_splitted[3]}'
            if pmr == demultiplex:
                p5 = line_splitted[8]
                p3 = line_splitted[9]
                return f'{p5}...{p3}'

if __name__ == '__main__':
    main(sys.argv)

