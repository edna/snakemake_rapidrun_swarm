__author__ = "Pierre-Edouard GUERIN"
__date__ = "2020/02/06"
__version__ = "$Revision: 1.0"


#===============================================================================
#INFORMATIONS
#===============================================================================
"""
CNRS - EPHE - 2020
GUERIN pierre-edouard

description:
convert an OTU tsv file into OTU fasta file

input:
OTU tsv file

output:
OTU fasta file

usage:
python3 OTU_table2fasta.py 02_otu_table/Fakarava/mamm/all.otu.table 03_otu_fasta/Fakarava/mamm/all.otu.fasta


"""


###############################################################################
# MODULES
###############################################################################

import os
import sys
import pandas


#===============================================================================
#MAIN
#===============================================================================
otu_table = sys.argv[1]
otu_fasta= sys.argv[2]

#print(otu_table,otu_fasta)
#otu_table="02_otu_table/Fakarava/mamm/all.otu.table"
#otu_fasta="blblb"

df =pandas.read_csv(otu_table, sep="\t")
new_name = df.amplicon
fa = [None] * df.shape[0]*2
fa[::2] = "> "+ new_name
fa[1::2] = df.sequence
## write fasta file output
sourceFile= open(otu_fasta,"w+")
print(*fa,sep="\n",file=sourceFile)
sourceFile.close()
