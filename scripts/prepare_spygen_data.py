###############################################################################
# usage
## python3 scripts/prepare_spygen_data.py --datFolder resources/tutorial/tutorial_rapidrun_data/ngs/testmaleplofakarava/rapidrun_metadata/ \
## --aliceFile cor_tags_all_markers.csv \
## --projetName notrProjet


###############################################################################
# notice
## aliceFile is parsed to seek marker, run and projet name
##
## datFolder contains .dat files the name of these .dat files must 
## match marker name into aliceFile (it is not case sensitive)
## for instance marker "mamm" must have the corresponding file Mamm.dat or
##  mamm.dat
##
## From aliceFile marker, run and projet, the program seek into corresponding 
## marker .dat file the column tag to get the plaque position
##
## At the end the program print a CSV ;-separated table with header
## "projet;marker;run;plaque;sample"
## which will be required for demultiplexing step
###############################################################################

###############################################################################
# modules
import pandas
import sys
import argparse
import os


###############################################################################
# arguments
parser = argparse.ArgumentParser(description='prepare rapidrun allsamples CSV file')
parser.add_argument("-d","--datFolder", type=str, help="Folder of the dat files")
parser.add_argument("-a","--aliceFile", type=str, help="CSV Alice file")
parser.add_argument("-p","--projetName", type=str, help="name of the project")
args = parser.parse_args()

datFolder = args.datFolder
aliceFile = args.aliceFile
projetName = args.projetName

#datFolder='resources/test/test_rapidrun_data/ngs/rapidrun_metadata/'
#datFolder='resources/tutorial/tutorial_rapidrun_data/ngs/testmaleplofakarava/rapidrun_metadata/'


datFolderList = os.listdir(datFolder)
datFiles = {}
for fichier in datFolderList:
    if ".dat" in fichier:
        datMarker = fichier.split(".")[0].lower()
        datFiles[datMarker]= datFolder+"/"+fichier
datDic = {}
for key in datFiles:
    datDic[key] = pandas.read_csv(datFiles[key], sep='\t', names=["plaque", "plaque1", "tag", "primerF","primerR", "description"])

alice = pandas.read_csv(aliceFile, sep=';')


###############################################################################
# main
uniqMarkers=alice.marker.unique()
for marker in uniqMarkers:
    #print(marker)
    aliceMarker = alice.loc[(alice['marker'] == marker)]
    uniqRunsMarker = aliceMarker.Run.unique()
    for run in uniqRunsMarker:
        #print(run)        
        aliceRun = aliceMarker.loc[(aliceMarker['Run'] == run)]
        if aliceRun.tag.is_unique is not True:
            print("File "+aliceFile+" is wrong : tag not unique for maker "+marker+" run "+run)
            sys.exit(0)
        count_blank=0
        for i in datDic[marker].index:
            in_alice=False
            for j in aliceRun.index:
                if aliceRun['tag'][j] == datDic[marker]['tag'][i]:
                    in_alice=True
                    plaque = datDic[marker]['plaque'][i]
                    sample = aliceRun['Sample'][j]
                    if "SPY" in sample:
                        projet=projetName
                    elif "Other" in sample or "OTHER" in sample or "Other" in sample:
                        projet="Other"
                    else:
                        projet=projetName
                    print(plaque, run, sample, projet, marker, sep=";")
                    break
            if not in_alice:
                plaque = datDic[marker]['plaque'][i]
                projet = "Blank"
                count_blank+=1
                sample="blank_"+marker+"_"+run+"_"+str(count_blank)
                print(plaque, run, sample, projet, marker, sep=";")                

#print(alice['Run'])
#print(dat['plaque'])
