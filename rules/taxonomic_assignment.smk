__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## assigns sequences to taxa
rule taxonomic_assignment:
    input:
        rules.otu_fasta.output
    output:
        Path('results', config['subfolders']['taxonomic_assignment'],'{projmark}/otu_genbank.tag.u.fasta')
    threads:
        workflow.cores * 0.5
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['taxonomic_assignment'], '{projmark}/ecotag.log')
    params:
        db = get_bdr,
        fasta = lambda w: config['reference'][list({w.projmark})[0].split('/')[1]]['fasta'],
        min_identity = config['ecotag']['min_identity']
    shell:
        '''
        mkdir -p $(dirname {output})
        [[ -s {input} ]] && ecotag -d {params.db} -R {params.fasta} -m {params.min_identity} {input} > {output} 2> {log} || touch {output}
        '''

if config['custom_baseref']:
    rule custom_bdr_taxonomic_assignment:
        input:
            get_custom_assignment_input
        output:
            Path('results', config['subfolders']['taxonomic_assignment'], '{projmark}/otu_custom.tag.u.fasta')
        threads:
            workflow.cores * 0.5
        conda:
            '../envs/obitools_envs.yaml'
        container:
            config["singularity"]["obitools"]
        log:
            Path('logs', config['subfolders']['taxonomic_assignment'], '{projmark}/ecotag.log')
        params:
            folder = lambda w: config['custom_ref'][list({w.projmark})[0].split('/')[1]]['taxonomy_folder'],
            fasta = lambda w: config['custom_ref'][list({w.projmark})[0].split('/')[1]]['fasta']
        shell:
            '''
            mkdir -p $(dirname {output})
            [[ -s {input} ]] && ecotag -t {params.folder} -R {params.fasta} {input} > {output} 2> {log} || touch {output}
            '''
