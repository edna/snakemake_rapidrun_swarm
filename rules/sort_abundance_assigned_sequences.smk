__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## The sequences can be sorted by decreasing order of count
rule sort_abundance_assigned_sequences:
    input:
        rules.remove_annotations.output
    output:
        Path('results', config['subfolders']['sort_abundance'], '{projmark}/otu_{bdr}.s.a.t.u.fasta')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    log:
        Path('logs', config['subfolders']['sort_abundance'], '{projmark}/otu_{bdr}.s.a.t.u.log')
    shell:
        '''
        mkdir -p $(dirname {output})
        obisort -k count -r {input} > {output} 2> {log}
        '''
