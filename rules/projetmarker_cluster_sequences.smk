__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## Clustering
rule clustering:
    input:
        rules.projetmarker_dereplicate_sequences.output
    output:
        representatives = temp(Path('results', config['subfolders']['clustering'], '{projmark}/cluster_representatives.fasta')),
        struct = Path('results', config['subfolders']['clustering'], '{projmark}/all.struct'),
        stats = Path('results', config['subfolders']['clustering'], '{projmark}/all.stats'),
        swarm = Path('results', config['subfolders']['clustering'], '{projmark}/all.swarm')
    threads: 4
    container:
        config["singularity"]["ednatools"]
    conda:
        '../envs/env_swarm3.yaml'
    params:
        resolution = config['swarm']['resolution']
    log:
        Path('logs', config['subfolders']['clustering'], '{projmark}.log')
    message:
        '(swarm v3) clustering:{wildcards.projmark}'
    shell:
        '''
        mkdir -p $(dirname {output});
        swarm -d {params.resolution} --fastidious -t {threads} --usearch-abundance \
        --internal-structure {output.struct} -s {output.stats} \
        --seeds {output.representatives} -o {output.swarm} < {input} 2> {log}
        '''
