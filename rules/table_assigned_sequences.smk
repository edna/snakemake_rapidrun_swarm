__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## Generate a table final results
rule table_assigned_sequences:
    input:
        rules.sort_abundance_assigned_sequences.output
    output:
        Path('results', config['subfolders']['table_assignment'], '{projmark}/{projet}_{marker}_otu_{bdr}_ecotag.tsv')
    log:
        Path('logs', config['subfolders']['table_assignment'], '{projmark}/{projet}_{marker}_otu_{bdr}_ecotag.log')
    conda:
        '../envs/obitools_envs.yaml'
    container:
        config["singularity"]["obitools"]
    shell:
        '''
        obitab -o {input} > {output} 2> {log}
        '''

rule mv_otu_table:
    input:
        table = rules.otu_table.output,
        fasta = rules.otu_fasta.output
    output:
        Path('results', config['subfolders']['table_assignment'], '{projmark}/{projet}_{marker}_table_motu.tsv')
    log:
        Path('logs', config['subfolders']['table_assignment'], '{projmark}/{projet}_{marker}_table_motu.log')
    shell:
        '''
        mv {input.table} {output}
        echo 'move {input.table} --> {output}' > {log}
        '''
