__author__ = ["Morgane Bruno"]
__licence__= "MIT"

##OTU table
rule otu_table:
    input:
        uchime = rules.remove_chimera_sequences.output.uchime,
        representatives = rules.sort_representatives.output,
        stats = rules.clustering.output.stats,
        swarm = rules.clustering.output.swarm,
        assignment = rules.remove_chimera_sequences.output.assignment,
        fasta = get_sample_fasta
    output:
        Path('results', config['subfolders']['otu_table'], '{projmark}/all.otu.csv')
    log:
        Path('logs', config['subfolders']['otu_table'], '{projmark}/otu_contingency_table.log')
    conda:
        '../envs/env_scripts_python.yaml'
    resources:
        job = 1
    threads: 1
    message:
        '(python script) Create contingency table: {wildcards.projmark}'
    shell:
        '''
        mkdir -p $(dirname {output})
        python3 scripts/OTU_contingency_table.py \
        {input.representatives} \
        {input.stats} \
        {input.swarm} \
        {input.uchime} \
        {input.assignment} \
        {input.fasta} > {output} 2> {log}
        '''

