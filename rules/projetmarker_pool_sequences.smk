__author__ = ["Morgane Bruno"]
__licence__ = "MIT"

## Pool sequences from same projet/marker
rule pool_sequences:
    input:
        expand(Path('results', config['subfolders']['sample_dereplicate'], '{demultiplex}.fasta').as_posix(), demultiplex = list_demultiplex())
    output:
        temp(expand(Path('results', config['subfolders']['pool_sequences'], '{projmark}/all.fasta').as_posix(), projmark = list_projmark()))
    log:
        Path('logs', config['subfolders']['pool_sequences'], 'pool_sequences.log')
    resources:
        job = 1
    threads: 1
    params:
        projmark = list_projmark(),
        in_subfold = config['subfolders']['sample_dereplicate'].split('/')[2],
        out_subfold = config['subfolders']['pool_sequences'].split('/')[2]
    message:
        "Pool sequences from same projet/marker"
    shell:
        '''
        for out_dir in $(dirname {output})
        do
            in_dir=$(echo $out_dir | sed 's/{params.out_subfold}/{params.in_subfold}/')
            files=$(ls ${{in_dir}}/*.fasta)
            cat $files > ${{out_dir}}/all.fasta
            echo "info :: Pool $files in ${{out_dir}}/all.fasta" >> {log}
        done
        '''


