# Changelog
All notable changes to this project will be documented in this file.

## [1.2.0]
### Added

* `config/config_template.yaml`
* `rules/common.smk` with helper functions
* `scripts/write_demultiplex_files.py` python script to create files used in demultiplexing step
* `scripts/prepare_spygen.R`

### Changed
* Clean snakefile: moving python code between rules in python scripts and common.smk fike
* Correction of errors identified by the code quality checker `snakemake --lint`
* Correction: The first sample of all_samples is now analysed

### Removed
* Old config file
* Unused rules


## [1.1.5]
### Added
- custom reference database
- rules taxonomic assignment custom ref
- `scripts/prepare_spygen_data.py` From SPYGEN Alice's file {marker}, {run} and {projet}, the program seek into corresponding  marker .dat file the column tag to get the plaque position in order to attribute the right {sample} at demultiplexing step

### Changed
- configfile handles custom reference database
- Fix conda ram issue in envs/env_obitools.yaml

### Removed
- useless quality sequences steps 

## [1.1.4]
### Added
- fastqc rule
- fastqc conda env
- documentation quickstart step
- resources test folder

### Changed
- name of results subfolders
- add fastqc item to configfiles

### Removed
- deprecated config files

## [1.1.3]
### Added
- "spygen origine" format (one single `.dat` file for one single `.fastq` file) can be processed
- UNIT TEST `01_settings/readwrite_rapidrun_demultiplexing.py` barcode_tags_duplicated.csv has exactly 2 columns (original, duplicated)
- TEST dataset to check in few seconds if the whole workflow works fine on RAPIDRUN case
- TEST dataset to check in few seconds if the whole workflow works fine on CLASSIC case
- copy and rename files generated from 19_otu_table and 24_table_assigned_sequences to results folder
- resources job: number of parallelized jobs are limited by the resource "job"
- `scripts/prepare_spygen_data.py` generates the all_samples.csv file from SPYGEN "standard" information file and {marker}.dat files
- TEST integrity of `.dat` files
- 

### Changed
- all worklows are merged into a single one
- results subfolders are generated automatically
- unique `scripts`, `rules` and `envs` folders
- Demultiplex: no_indel option cutadapt to prevent insertion between tags and primers
- Alternative workflow to perform taxonomic assignment wihtout `ecotag`
- Fix wildcards in CLASSIC mode
- convert `scripts/scripts/OTU_contingency_table.py` code from python2 into python3
- update cutadapt version 2 to cutadapt version 3.2
- Fix SettingWithCopyWarning pandas dataframe
- factorisation folder results generation
- Fix data export

### Removed
- old folders and scripts 01_settings, 02_assembly, 03_demultiplex, etc...
- old preexisting subfolders into results folder
- `clean.sh` script which is now useless
- `rename.sh` script which is replaced by a rule in Snakefile

## [1.1.2] - 21th sep 2020
### Added
- can handle 2 different types of input format CLASSIC and RAPIDRUN
- tutorial to process data in CLASSIC format based on a subset of Rhone project data


## [1.1.1] - 8th sep 2020
### Added
- data test
- tutorial

### Changed
- The demultiplexing method is more efficient. First it demultiplexes linked barcode tags. For each run, every linked barcode are seeked only once. Second it trimms primer 5' and 3' for each sample fastq files. Primer and barcode tags are processed in forward and reverse complement.


## [1.1.0] - 1st sep 2020
### Added
- Conda v4.8.2 envs
- Autorship
- license MIT

### Changed
- new structure for distribution based on snakemake recommandation
- all_samples.tsv rapidrun input is now in a `csv` format with `;` as separator


## [1.0.1] - 31th aug 2020
### Added
- blacklist runs or projects from 'rapidrun' .tsv file
- convert otu.table into otu.fasta at step 05_clustering
- step 06_assignment taxonomic assignment

### Changed
- configfile has blacklist keys
- configfile has reference database information
- final output is `.csv` instead of `.table`
- script `rename.sh` to automatically rename output files with a prefix

### Removed
- old rapidrun tables `.rrr`

## [1.0.0] - 6th feb 2020
### Added
- Complete workflow 04_cat_quality dedicated to generate .qual files

### Changed
- main.sh argument CONFIGFILE is constant

### Removed
- Files .qual are not generated anymore at demultiplexing step

