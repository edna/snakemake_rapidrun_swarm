__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__license__ = "MIT"

## Dereplicate and fix name of sequences (have to end with ';')
rule projetmarker_dereplicate_sequences:
    input:
        Path('results', config['subfolders']['pool_sequences'], '{projmark}/all.fasta')
    output:
        Path('results', config['subfolders']['projetmarker_dereplicate'], '{projmark}/all.uniq.fasta')
    threads: 1
    log:
        Path('results', config['subfolders']['projetmarker_dereplicate'], '{projmark}/vsearch.log')
    container:
        config["singularity"]["ednatools"]
    conda:
        '../envs/env_vsearch.yaml'
    shell:
        '''
        mkdir -p $(dirname {output})
        if [[ -s {input} ]]; then vsearch --derep_fulllength {input} --sizein --sizeout --fasta_width 0 --output {output} > /dev/null 2> {log};
        sed -i '1~2 s/$/;/g' {output};
        else touch {output}; fi;
        '''
