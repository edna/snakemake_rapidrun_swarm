#!/bin/bash
barcode_dir=$1
fq_dir=$2
threads=$3
min_len=$4
all_run=${@: 5}

for run in $all_run
do
    barcode=$barcode_dir'/'$run'.fasta'
    fq=$fq_dir'/'$run'.fastq'
    echo $fq
    echo $barcode
    echo '--------------------------------------------'
    echo 'info :: Running cutadapt - Run: '$run
    echo '--------------------------------------------'
    cutadapt -j $threads -m $min_len --revcomp -O 8 --discard-untrimmed -g file:$barcode $fq -o {name}.fastq
done
