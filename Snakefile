#===============================================================================
#HEADER
#===============================================================================
__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__credits__ = ["Pierre-Edouard Guerin", "Virginie Marques", "Morgane Bruno"]
__license__ = "MIT"
__version__ = "1.2.0"
__maintainer__ = "Morgane Bruno"
__email__ = "morgane.bruno@cefe.cnrs.fr"
__status__ = "Production"
"""
Codes for scientific papers related to metabarcoding studies

AUTHORS
=======
* Morgane Bruno           | morgane.bruno@cefe.cnrs.fr
* Pierre-Edouard Guerin   | pierre-edouard.guerin@cefe.cnrs.fr
* Virginie Marques        | virginie.marques@cefe.cnrs.fr
* CNRS/CEFE, CNRS/MARBEC  | Montpellier, France
* 2018-2020


DESCRIPTION
===========
This is a Snakefile using SNAKEMAKE workflow management system
From sample description files .dat files, config.yaml, rapidrun.tsv
it will return a demultiplexing.csv file. The output contains all required
wildcards en related information necessary to run the next workflow steps.
"""

from snakemake.utils import min_version
min_version("6.10.0")
from pathlib import Path
import os, json

###############################################################################
# WORKFLOW
###############################################################################

#_rules
include: "rules/common.smk"
# Merge paired-end
include: "rules/merge_fastq.smk"
# Demultiplexing
include: "rules/cutadapt.smk"
# Clean data
include: "rules/trim_primers.smk"
include: "rules/discard_iupac_ambiguous_sequences.smk"
include: "rules/sample_dereplicate_sequences.smk"
include: "rules/projetmarker_pool_sequences.smk"
include: "rules/projetmarker_dereplicate_sequences.smk"
# Clustering
include: "rules/projetmarker_cluster_sequences.smk"
include: "rules/sort_representatives.smk"
# Remove chimeras
include: "rules/remove_chimera_sequences.smk"
# Construct sequence table
include: "rules/otu_table.smk"
# Construct sequence fasta
include: "rules/otu_fasta.smk"
# Taxonomic assignment
include: "rules/taxonomic_assignment.smk"
include: "rules/remove_annotations.smk"
include: "rules/sort_abundance_assigned_sequences.smk"
include: "rules/table_assigned_sequences.smk"

#_targets
rule all:
    input:
        get_targets()


