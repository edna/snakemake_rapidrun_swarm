__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__licence__= "MIT"

##OTU fasta
rule otu_fasta:
    input:
        rules.otu_table.output
    output:
        Path('results', config['subfolders']['otu_fasta'], '{projmark}/all.otu.fasta')
    log:
        Path('logs', config['subfolders']['otu_fasta'], '{projmark}/otu_to_fasta.log')
    conda:
        '../envs/env_scripts_python.yaml'
    resources:
        job = 1
    threads: 1
    message:
        '(python script) Create fasta file with otu sequences: {wildcards.projmark}'
    shell:
        '''
        mkdir -p $(dirname {output})
        python3 scripts/OTU_table2fasta.py {input} {output} 2> {log}
        '''
