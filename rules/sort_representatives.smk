__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## Sort representatives
rule sort_representatives:
    input:
        rules.clustering.output.representatives
    output:
        Path('results', config['subfolders']['sort_representatives'], '{projmark}/cluster_representatives.sorted.fasta')
    threads:
        1
    container:
        config["singularity"]["ednatools"]
    conda:
        '../envs/env_vsearch.yaml'
    log:
        Path('logs', config['subfolders']['sort_representatives'], '{projmark}/cluster_representatives.sorted.log')
    message:
        '(vsearch) Sort by size cluster representatives: {wildcards.projmark}/cluster_representatives.fasta'
    shell:
        '''
        mkdir -p $(dirname {output})
        vsearch --fasta_width 0 --sortbysize {input} --output {output} &> {log}
        '''
