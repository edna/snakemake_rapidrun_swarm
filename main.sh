###############################################################################
## Here, we reproduce the bioinformatics workflow used by SPYGEN 
## We improve its performance by adding parallelization and containerazition features.
##
## This workflow generates species environmental presence from raw eDNA data. 
##
## This workflow use the workflow management system SNAKEMAKE.
## 
##
## Authors : GUERIN Pierre-Edouard, MARQUES Virginie
## Montpellier 2019-2020
## 
###############################################################################
## Usage:
##    CORE=16
##    CONFIGFILE=path/to/my/configfile.yaml
##    bash main.sh $CORES $CONFIGFILE
##
##
###############################################################################
CONFIGFILE=$1
CORES=$2

##
#CONFIGFILE="config/config_test.yaml"
#CONFIGFILE="config/config_tutorial_rapidrun.yaml"
#CONFIGFILE="config/config_tutorial_classic.yaml"
CONFIGFILE="config/config_test_rapidrun.yaml"
#CORES=16

#snakemake --configfile config/config_test_rapidrun.yaml -s Snakefile --cores 8 --use-conda
#snakemake --configfile config/config_tutorial_rapidrun.yaml -s Snakefile --cores 8 --use-conda
#snakemake --configfile config/config_tutorial_classic.yaml -s Snakefile --cores 8 --use-conda

snakemake --use-conda --configfile ${CONFIGFILE} --cores ${CORES}


############################################################################### 
## clean
#snakemake --configfile $CONFIGFILE -s Snakefile --delete-all-output --dry-run
#snakemake --configfile "../"$CONFIGFILE -s Snakefile --delete-all-output --rerun-incomplete
## test
#conda activate snakemake_rapidrun
#rm -rf results; mkdir results; snakemake --use-conda --configfile config/config_test_rapidrun.yaml --cores 4

