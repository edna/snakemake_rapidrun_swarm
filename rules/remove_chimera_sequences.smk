__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__licence__ = "MIT"

## Chimera checking
rule remove_chimera_sequences:
    input:
        rules.sort_representatives.output
    output:
        uchime = Path('results', config['subfolders']['remove_chimera'], '{projmark}/cluster_representatives.uchime'),
        assignment = Path('results', config['subfolders']['remove_chimera'], '{projmark}/cluster_representatives.assignement')
    log:
        Path('logs', config['subfolders']['remove_chimera'], '{projmark}/cluster_representatives.uchime.log')
    conda:
        '../envs/env_vsearch.yaml'
    threads: 1
    message:
        '(vsearch) Remove chimera: {wildcards.projmark}/cluster_representatives.fasta'
    shell:
        '''
        mkdir -p $(dirname {output})
        vsearch --uchime_denovo {input} --uchimeout {output.uchime} &> {log}
        if grep -q . {output.uchime}
        then
        	grep "^>" {input} | sed -e 's/^>//' -e 's/;size=/\t/' -e 's/;$/\t100.0\tNA\tNA/' > {output.assignment}
        else
 	        touch {output.assignment}
        fi
        '''
