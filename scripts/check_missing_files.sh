###############################################################################
## Cutadapt don't generate file for barcode that are not detected
## We need for snakemake to generate empty files when barcode are not detected
## into a sample of a given run
##
##
## Authors : GUERIN Pierre-Edouard, MARQUES Virginie
## Montpellier 2019-2020
###############################################################################


FASTA=$1
#FASTA="../results/01_settings/barcodes/181102_SND405_A_L002_AIMI-49.fasta"

for fa in `grep "^>" ${FASTA} | cut -c2-`;
do
FASTQ=$fa.fastq
if [ -f "$FASTQ" ]; then
    echo "$FASTQ exists."
else 
    echo "$FASTQ does not exist. Generating empty files"
    touch $FASTQ
fi
done