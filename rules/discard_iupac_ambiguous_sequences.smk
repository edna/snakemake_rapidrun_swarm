__author__ = ["Pierre-Edouard Guerin", "Morgane Bruno"]
__licence__ = "MIT"

## Discard sequences containing Ns, convert to fasta
rule discard_iupac_ambiguous_sequences:
    input:
        rules.trim_primers.output
    output:
        Path('results', config['subfolders']['discard_ambiguous'], '{demultiplex}.fasta')
    log:
        Path('logs', config['subfolders']['discard_ambiguous'], '{demultiplex}.log')
    conda:
        '../envs/env_vsearch.yaml'
    container:
    	config['singularity']['ednatools']
    resources:
        job = 1
    threads: 1
    message:
        "(vsearch) Discard sequences containing Ns {wildcards.demultiplex}"
    shell:
        '''
        mkdir -p $(dirname {output})
        if [ -s {input} ]
        then
            vsearch --fastq_filter {input} --fastq_maxns 0 --fastaout {output} &> {log}
        else
            touch {output}
            echo "The input file {input} is empty." >> {log}
        fi
        '''
